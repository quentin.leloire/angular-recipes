import { Component, OnInit } from '@angular/core';
import { Recipe } from 'src/models/Recipe';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-list-recipes',
  templateUrl: './list-recipes.component.html',
  styleUrls: ['./list-recipes.component.scss']
})
export class ListRecipesComponent implements OnInit {

  recipes: Recipe[];

  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.getRecipes().subscribe(data => {
      this.recipes = data
      console.log(this.recipes);
    });
  }
}