import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hiddenPassword: boolean = true;
  isLoged: boolean = false;

  login: string;
  password: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  logging(): void {
    if ((this.login == 'utilisateur' && this.password == 'mdp') ) {
      this.isLoged = true;
      this.router.navigate(["/recipes"]);
    }
    else {
      alert("Identifiant ou mot de passe incorrect !");
    }
  }
}
