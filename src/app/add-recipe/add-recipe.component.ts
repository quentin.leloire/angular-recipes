import { Component, OnInit } from '@angular/core';
import { Recipe } from 'src/models/Recipe';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../services/data.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.scss']
})

export class AddRecipeComponent implements OnInit {

  units: string[] = [
    "pincée",
    "g",
    "mg",
    "cl",
    "ml",
  ];

  types: string[] = [
    "Entrée",
    "Soupe",
    "Plat",
    "Dessert",
  ];

  form: FormGroup;
  recipe: Recipe;

  constructor(private http: HttpClient, private builder: FormBuilder, private dataService: DataService) {
    this.createForm();
  }

  createForm() {
    this.form = this.builder.group({
      type: ['', Validators.required],
      name: ['', Validators.required],
      price: [null, Validators.required],
      duration: [null, Validators.required],
      description: ['', Validators.required],
      img: ['', Validators.required],
    });
  }

  addRecipe(type, name, description, price, duration, img) {
    this.recipe = {
      type: type,
      name: name,
      description: description,
      price: price,
      duration: duration,
      img: img,
    };
    this.dataService.addRecipe(this.recipe);
  }

  ngOnInit() {
  }

}