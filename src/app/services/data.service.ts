import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from 'src/models/Recipe';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  apiUrl = 'http://localhost:3000/api/recipes';

  constructor(private http: HttpClient) { }

  getRecipes() {
    return this.http.get<Recipe[]>(`${this.apiUrl}`);
  }
  
  getRecipe(_id: number) {
    return this.http.get<Recipe[]>(`${this.apiUrl}/${_id}`);
  }

  addRecipe(recipe: Recipe) {
    return this.http.post<Recipe>(`${this.apiUrl}`, recipe)
      .subscribe(
        result => {
          alert("Recette ajoutée !");
          return result;
        },
        error => {
          alert("Champs manquant(s) !");
          return error;
        }
      );
  }

  public updateRecipe(recipe: Recipe){
    return this.http.put(`${this.apiUrl}/${recipe._id}`,recipe);
  }
}
