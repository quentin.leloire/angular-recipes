export interface Ingredient {
    id: number;
    name: string;
    quantity: number;
    unit: string;
}

export interface Recipe {
    _id?: number;
    type: string;
    name: string;
    price: number;
    duration: number;
    description: string;
    img: string;
    ingredients?: Ingredient[];
}