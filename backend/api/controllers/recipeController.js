'use strict';

var mongoose = require('mongoose'),
Recipe = mongoose.model('Recipes');

exports.listRecipes = function(req, res) {
    Recipe.find({}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.createRecipe = function (req, res) {
    var recipe = new Recipe(req.body);
    recipe.save(function (err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};

exports.readRecipe = function (req, res) {
    Recipe.findById(req.params.taskId, function (err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};

exports.updateRecipe = function (req, res) {
    Recipe.findOneAndUpdate({ id: req.params.id }, req.body, { new: true }, function (err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};

exports.deleteRecipe = function (req, res) {
    Recipe.remove({
        _id: req.params.taskId
    }, function (err, recipe) {
        if (err)
            res.send(err);
        res.json({ message: 'Recette supprimée' });
    });
};