'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/*
var IngredientSchema = new Schema({
    id: {
        type: Number,
        required: "l'id ingrédient requis"
    },
    name: {
        type: String,
        required: "nom ingrédient requis"
    },
    quantity: {
        type: Number,
    },
    unit: {
        type: String,
    }
});
*/
var RecipeSchema = new Schema({
    type: {
        type: String,
        required: "type recette requis"
    },
    name: {
        type: String,
        required: "nom recette requis"
    },
    price: {
        type: Number,
        required: "prix requis"
    },
    duration: {
        type: Number,
        required: "durée requise"
    },
    description: {
        type: String,
        required: "description requise"
    },
    img: {
        type: String,
        required: "image requise"
    },
/*    ingredients: [IngredientSchema]*/
});

module.exports = mongoose.model('Recipes', RecipeSchema);