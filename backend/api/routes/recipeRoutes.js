'use strict';
module.exports = function(app) {
  var recipeList = require('../controllers/recipeController');


  app.route('/api/recipes')
    .get(recipeList.listRecipes)
    .post(recipeList.createRecipe);


  app.route('/api/recipes/:recipeId')
    .get(recipeList.readRecipe)
    .put(recipeList.updateRecipe)
    .delete(recipeList.deleteRecipe);
};