FROM node
WORKDIR /app
COPY . /app/
RUN npm install
RUN ng build --prod
FROM nginx
COPY --from=0 /app/dist/ /usr/share/nginx/html

COPY /app/backend/ /opt/
RUN npm run start &

#COPY ./nginx.conf /etc/nginx/conf.d/default.conf